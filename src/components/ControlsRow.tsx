import React from "react";

type Props = {
  header: string | Function;
  children: any;
};

const ControlsRow = (props: Props) => {
  return (
    <div className="controls-row">
      <div className="controls-row-header">
        {typeof props.header === "string" ? props.header : props.header()}
      </div>
      <div className="controls-row-children">{props.children}</div>
    </div>
  );
};

export default ControlsRow;
