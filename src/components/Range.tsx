import React, { useRef, useState, useEffect, useCallback } from "react";
import useMouseDrag from "../hooks/useMouseDrag";
import useVideo from "../hooks/useVideo";
import "./Range.css";
import { useControlsState } from "../stores/controls";

type Props = {
  start: number;
  end: number;
  duration: number;
  currentTime: number;
  onCurrentTime: (val: number) => void;
  onStart: (val: number) => void;
  onEnd: (val: number) => void;
  frameSizeInFract: number;
  videoSrc?: string;
};

const Range = (props: Props) => {
  const { start, end, onStart, onEnd, frameSizeInFract } = props;
  const { minipreviews } = useControlsState();
  const wrapper = useRef<HTMLDivElement | null>(null);
  const startHandle = useRef<HTMLButtonElement | null>(null);
  const endHandle = useRef<HTMLButtonElement | null>(null);
  const [focus, setFocus] = useState<"start" | "end" | null>(null);

  const startCutRef = useRef<HTMLVideoElement | null>(null);
  const endCutRef = useRef<HTMLVideoElement | null>(null);
  const startSeek = useVideo(startCutRef).seek;
  const endSeek = useVideo(endCutRef).seek;

  const fractToTime = useCallback((fract: number) => fract * props.duration, [
    props.duration
  ]);

  const boundedSetStart = (val: number) => {
    onStart(val < 0 ? 0 : val);
    startSeek(fractToTime(props.start));
  };

  const boundedSetEnd = (val: number) => {
    onEnd(val > 1 ? 1 : val);
    endSeek(fractToTime(props.end));
  };

  const onMouseDownStart = useMouseDrag(boundedSetStart, wrapper);
  const onMouseDownEnd = useMouseDrag(boundedSetEnd, wrapper);
  const onMouseDownTime = useMouseDrag(
    x => props.onCurrentTime(x * props.duration),
    wrapper
  );

  useEffect(() => {
    const LEFT = 37;
    const RIGHT = 39;
    const keyHandler = (e: KeyboardEvent) => {
      e.stopPropagation();
      switch (e.keyCode) {
        case LEFT:
          if (focus === "start") {
            boundedSetStart(start - frameSizeInFract);
          } else if (focus === "end") {
            boundedSetEnd(end - frameSizeInFract);
          }
          break;
        case RIGHT:
          if (focus === "start") {
            boundedSetStart(start + frameSizeInFract);
          } else if (focus === "end") {
            boundedSetEnd(end + frameSizeInFract);
          }

          break;
        default:
          break;
      }
    };
    window.addEventListener("keydown", keyHandler);
    return () => {
      window.removeEventListener("keydown", keyHandler);
    };
  });

  return (
    <div ref={wrapper} className="range">
      {minipreviews.length ? (
        <div
          className="previews-wrap"
          style={{
            backgroundImage: minipreviews.map(url => `url(${url})`).join(","),
            backgroundPosition: minipreviews
              .map(
                (_, i) =>
                  i *
                    ((wrapper.current?.clientWidth ?? 1000) /
                      minipreviews.length) +
                  "px"
              )
              .join(",")
          }}
        />
      ) : (
        <div className="rail" />
      )}
      <div
        className="dimmer dimmer-start"
        style={{
          width: `${100 * start}%`
        }}
      />
      <div
        className="dimmer dimmer-end"
        style={{
          width: `${100 * (1 - end)}%`
        }}
      />
      <div
        className="seeker"
        style={{
          left: `${100 * (props.currentTime / props.duration)}%`
        }}
        onMouseDown={onMouseDownTime}
      >
        <div className="seeker-stem" />
        <div className="seeker-base" />
      </div>
      <button
        ref={startHandle}
        style={{
          left: `${100 * start}%`
        }}
        className="range-handle"
        onMouseDown={onMouseDownStart}
        onFocus={() => setFocus("start")}
        onBlur={() => setFocus(null)}
      >
        <video src={props.videoSrc} ref={startCutRef} height="100" />
      </button>
      <button
        ref={endHandle}
        style={{ left: `${100 * end}%` }}
        className="range-handle"
        onMouseDown={onMouseDownEnd}
        onFocus={() => setFocus("end")}
        onBlur={() => setFocus(null)}
      >
        <video src={props.videoSrc} ref={endCutRef} height="100" />
      </button>
    </div>
  );
};

export default Range;
