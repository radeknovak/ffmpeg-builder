import React, { FC } from "react";
import { useCropContext, cropToVideoCoords } from "../stores/crop";
import { buildCommand } from "../utils/command";
import { FileMeta, useControlsState } from "../stores/controls";

const isDefaultCrop = (
  fileMetadata: FileMeta,
  crop: {
    x: number;
    y: number;
    width: number;
    height: number;
  }
) =>
  crop.x === 0 &&
  crop.y === 0 &&
  fileMetadata.videoWidth <= crop.width &&
  fileMetadata.videoHeight <= crop.height;

const CommandView: FC = () => {
  const [state] = useCropContext();
  const controlsState = useControlsState();

  const {
    fileExtension,
    shortName,
    hue,
    saturation,
    fileMetadata,
    range,
    preset,
    active,
    ranges,
    disabledRanges,
    flip
  } = controlsState;

  if (!fileMetadata) return null;

  const fractToTime = (fract: number) => fract * fileMetadata.duration;
  const crop = cropToVideoCoords(state, fileMetadata);
  const options = {
    start: fractToTime(range.start),
    end: fractToTime(range.end),
    flip,
    ext: fileExtension,
    shortName,
    hueSat: {
      hue,
      saturation
    },
    name: fileMetadata.name,
    presetValue: preset,
    crop: isDefaultCrop(fileMetadata, crop) || !active.crop ? null : crop
  };

  // const command = buildCommand(options);

  const commands = ranges
    .filter((_, i) => !disabledRanges.has(i))
    .map(([start, end], i) =>
      buildCommand({
        ...options,
        shortName: `${fileMetadata.name}[part-${i}]`,
        start: fractToTime(start),
        end: fractToTime(end - start)
      })
    );

  const textCommands = commands.join("\n");

  return (
    <div style={{ display: "flex" }}>
      {/* <div
        style={{
          position: "relative",
          display: "inline-block",
          padding: "0.5rem 1rem"
        }}
      >
        <code>
          <pre>{command}</pre>
        </code>
        <button
          style={{ position: "absolute", right: 2, top: 2 }}
          onClick={() => navigator.clipboard.writeText(command)}
        >
          copy
        </button>
      </div> */}
      <div
        style={{
          position: "relative",
          display: "inline-block",
          padding: "0.5rem 1rem"
        }}
      >
        <code>
          <pre>{textCommands}</pre>
        </code>
        <button
          style={{ position: "absolute", right: 2, top: 2 }}
          onClick={() => navigator.clipboard.writeText(textCommands)}
        >
          copy
        </button>
      </div>
    </div>
  );
};

export default CommandView;
