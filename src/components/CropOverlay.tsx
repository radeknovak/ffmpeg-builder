import React, { useRef } from "react";
import { useCropContext } from "../stores/crop";
import useMouseDrag from "../hooks/useMouseDrag";

const CropOverlay = (props: { show: boolean }) => {
  const wrapper = useRef<HTMLDivElement | null>(null);
  const [{ topLeft, bottomRight, width, height }, dispatch] = useCropContext();

  const onMouseTopLeft = useMouseDrag((ratioX, ratioY, x, y) => {
    dispatch({
      type: "set_top_left",
      payload: { x, y, ratioX, ratioY }
    });
  }, wrapper);

  const onMouseTopRight = useMouseDrag((ratioX, ratioY, x, y) => {
    dispatch({
      type: "set_bottom_right",
      payload: { x, y, ratioX, ratioY }
    });
  }, wrapper);

  React.useLayoutEffect(() => {
    const bbox = wrapper.current?.getBoundingClientRect();
    if (!bbox || (bottomRight.x !== 0 && bottomRight.y !== 0)) return;
    dispatch({
      type: "set_top_left",
      payload: { x: 0, y: 0, ratioX: 0, ratioY: 0 }
    });

    dispatch({
      type: "set_bottom_right",
      payload: { x: bbox.width, y: bbox.height, ratioX: 1, ratioY: 1 }
    });
  });

  if (!props.show) return null;
  return (
    <div ref={wrapper} className="crop-overlay">
      <svg id="svg" width="100%" height="100%">
        <defs>
          <mask id="overlay" x="0" y="0" width="100%" height="100%">
            <rect
              x="0"
              y="0"
              width="100%"
              height="100%"
              fill="#fff"
              stroke="none"
            />
            <rect
              x={topLeft.x}
              y={topLeft.y}
              width={width}
              height={height}
              fill="#000"
              stroke="none"
            />
          </mask>
        </defs>

        <rect
          x="0"
          y="0"
          width="100%"
          height="100%"
          stroke="none"
          fill="rgba(0,0,0,0.8)"
          mask="url(#overlay)"
        />
      </svg>
      <button
        style={{
          left: topLeft.x,
          top: topLeft.y
        }}
        className="handle handle--crop"
        onMouseDown={onMouseTopLeft}
      />
      <button
        style={{
          left: bottomRight.x,
          top: bottomRight.y
        }}
        className="handle handle--crop"
        onMouseDown={onMouseTopRight}
      />
    </div>
  );
};

export default CropOverlay;
