import React, { FC } from "react";
import { useControlsState } from "../stores/controls";

const Filedetails: FC = () => {
  const controlsState = useControlsState();

  if (!controlsState.fileMetadata) return null;
  return (
    <details>
      <summary>Original file details</summary>
      <table className="stats">
        <tbody>
          {Object.entries(controlsState.fileMetadata).map(([prop, value]) => (
            <tr key={prop}>
              <td>{prop}</td>
              <td>{value}</td>
            </tr>
          ))}
          <tr></tr>
        </tbody>
      </table>
    </details>
  );
};

export default Filedetails;
