import React, { FC, useCallback, useRef, useState } from "react";
import useMouseDrag from "../hooks/useMouseDrag";
import useVideo from "../hooks/useVideo";
import { useControlsState } from "../stores/controls";
import "./Seekbar.css";

const Seekbar: FC<{
  videoSrc?: string;
  duration: number;
  currentTime: number;
  onCurrentTime: (val: number) => void;
}> = props => {
  const { onCurrentTime, duration } = props;
  const controlsState = useControlsState();

  const wrapper = useRef<HTMLDivElement>(null);
  const vidPreview = useRef<HTMLVideoElement>(null);
  const [previewTime, setPreviewTime] = useState(0);

  const setSeek = useVideo(vidPreview).seek;

  const fractToTime = useCallback(
    (fract: number) =>
      controlsState.fileMetadata
        ? fract * controlsState.fileMetadata.duration
        : 0,
    [controlsState.fileMetadata]
  );

  const onMouseMove = useMouseDrag((val: number) => {
    setPreviewTime(val < 0 ? 0 : val);
    setSeek(fractToTime(previewTime));
  }, wrapper);

  const handleClick = useCallback(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      if (!wrapper.current) return;
      const bbox = wrapper.current.getBoundingClientRect();
      const currentX = (e.pageX - bbox.left - window.scrollX) / bbox.width;

      onCurrentTime(currentX * duration);
    },
    [onCurrentTime, duration]
  );

  const onMouseDown = useMouseDrag(
    x => props.onCurrentTime(x * props.duration),
    wrapper
  );

  return (
    <>
      <div className="seekbar">
        <div
          className="seekbar-rail"
          ref={wrapper}
          onMouseMove={onMouseMove}
          onMouseDown={onMouseDown}
          onClick={handleClick}
        >
          <div
            className="seekbar-handle"
            style={{
              left: `${100 * (props.currentTime / props.duration)}%`
            }}
          />
          <div
            className="seekbar-rail-preview-wrapper"
            style={{ left: `calc(${100 * previewTime}% - 100px)` }}
          >
            <video src={props.videoSrc} ref={vidPreview} width="200" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Seekbar;
