import React, { FC, useCallback, useRef, useState } from "react";
import useMouseDrag from "../hooks/useMouseDrag";
import useVideo from "../hooks/useVideo";
import { useControlsState, useControlsDispatch } from "../stores/controls";
import "./Multicut.css";

const Multicut: FC<{
  videoSrc?: string;
}> = props => {
  const controlsState = useControlsState();
  const [previewTime, setPreviewTime] = useState(0);
  const [selectedMark, setSelectedMark] = useState<null | number>(null);

  const controlsDispatch = useControlsDispatch();

  const vidPreview = useRef<HTMLVideoElement>(null);
  const wrapper = useRef<HTMLDivElement>(null);

  const onMouseMove = useMouseDrag((val: number) => {
    setPreviewTime(val < 0 ? 0 : val);
    setSeek(fractToTime(previewTime));
  }, wrapper);

  const handleMarkMove = useMouseDrag((val: number) => {
    if (selectedMark === null) return;

    controlsDispatch({
      type: "MOVE_CUT_MARK",
      payload: { index: selectedMark, value: val }
    });
  }, wrapper);

  const setSeek = useVideo(vidPreview).seek;

  const fractToTime = useCallback(
    (fract: number) =>
      controlsState.fileMetadata
        ? fract * controlsState.fileMetadata.duration
        : 0,
    [controlsState.fileMetadata]
  );

  const deleteSelected = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    if (selectedMark === null) return;

    controlsDispatch({ type: "DELETE_CUT_MARK", payload: selectedMark });
    setSelectedMark(null);
  };

  const { marks, ranges } = controlsState;

  return (
    <>
      <div
        ref={wrapper}
        className="multicut-rail"
        onMouseMove={onMouseMove}
        onClick={() =>
          controlsDispatch({ type: "ADD_CUT_MARK", payload: previewTime })
        }
      >
        <div
          className="multicut-rail-mark multicut-rail-mark--current"
          style={{ left: `${100 * previewTime}%` }}
        />
        {ranges.map(([start, end], i) => (
          <div
            key={`${i}-${start}-${end}`}
            className="multicut-rangebox"
            style={{
              left: `${100 * start}%`,
              width: `${(end - start) * 100}%`
            }}
          />
        ))}
        {marks.map((mark, i) => (
          <div
            key={`${i}::${mark}`}
            className={`multicut-rail-mark ${
              selectedMark === i ? "multicut-rail-mark--selected" : ""
            }`}
            style={{ left: `${100 * mark}%` }}
            onClick={e => {
              // prevent setting mark
              e.stopPropagation();

              setSelectedMark(selectedMark === i ? null : i);
            }}
          />
        ))}
        <div
          className="multicut-rail-preview-wrapper"
          style={{ left: `calc(${100 * previewTime}% - 100px)` }}
        >
          <video src={props.videoSrc} ref={vidPreview} width="200" />
        </div>
        {selectedMark !== null && (
          <button
            onClick={deleteSelected}
            className="multicut-rail-btn"
            style={{ left: `calc(${100 * marks[selectedMark]}%)` }}
          >
            delete mark
          </button>
        )}
        {selectedMark !== null && (
          <div
            onMouseDown={handleMarkMove}
            className="multicut-rail-handle"
            style={{ left: `calc(${100 * marks[selectedMark]}%)` }}
          />
        )}
      </div>
    </>
  );
};

export default Multicut;
