import React from "react";
import { useCropContext, cropToVideoCoords } from "../stores/crop";
import { useControlsState } from "../stores/controls";

const Cropper = () => {
  const controlsState = useControlsState();
  const [state] = useCropContext();

  const fileMetadata = controlsState.fileMetadata;

  if (!fileMetadata) return null;

  const { x, y, width, height } = cropToVideoCoords(state, fileMetadata);
  return (
    <div>
      Crop:
      <label>
        x
        <input
          readOnly
          type="number"
          value={x}
          min={0}
          step={1}
          max={fileMetadata.videoWidth}
        />
      </label>
      <label>
        y
        <input
          readOnly
          type="number"
          value={y}
          min={0}
          step={1}
          max={fileMetadata.videoHeight}
        />
      </label>
      <label>
        width
        <input
          readOnly
          type="number"
          value={width}
          min={0}
          step={1}
          max={fileMetadata.videoWidth}
        />
      </label>
      <label>
        height
        <input
          readOnly
          type="number"
          value={height}
          min={0}
          step={1}
          max={fileMetadata.videoHeight}
        />
      </label>
    </div>
  );
};

export default Cropper;
