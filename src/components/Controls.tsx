import React, { FC } from "react";
import ControlsRow from "./ControlsRow";
import Cropper from "./Cropper";
import {
  PresetValues,
  useControlsDispatch,
  useControlsState
} from "../stores/controls";

const Controls: FC<{}> = () => {
  const controlsState = useControlsState();
  const controlsDispatch = useControlsDispatch();

  const handleStartChange = (val: number) => {
    controlsDispatch({ type: "SET_RANGE_START", payload: val });
  };
  const handleEndChange = (val: number) => {
    controlsDispatch({ type: "SET_RANGE_END", payload: val });
  };

  const fractToTime = (fract: number) =>
    controlsState.fileMetadata
      ? fract * controlsState.fileMetadata.duration
      : 0;

  const timeToFract = (time: number) =>
    controlsState.fileMetadata ? time / controlsState.fileMetadata.duration : 0;

  return (
    <div className="controls">
      <ControlsRow header="cut">
        <label htmlFor="time-from">from</label>
        <input
          id="time-from"
          type="number"
          value={fractToTime(controlsState.range.start)}
          onChange={({ target: { value } }) =>
            handleStartChange(timeToFract(Number(value)))
          }
        />
        <label htmlFor="time-to">to</label>
        <input
          id="time-to"
          type="number"
          value={fractToTime(controlsState.range.end)}
          onChange={({ target: { value } }) =>
            handleEndChange(timeToFract(Number(value)))
          }
        />
      </ControlsRow>
      <ControlsRow header="ranges">
        {controlsState.ranges.map(([start, end], i) => (
          <div key={`${i}-${start}---${end}`} className="range-control mr2">
            <label>
              <input
                type="checkbox"
                onChange={() =>
                  controlsDispatch({
                    type: controlsState.disabledRanges.has(i)
                      ? "REENABLE_RANGE"
                      : "DISABLE_RANGE",
                    payload: i
                  })
                }
                checked={!controlsState.disabledRanges.has(i)}
              />
              {fractToTime(start).toFixed(2)}s - {fractToTime(end).toFixed(2)}s
            </label>
          </div>
        ))}
      </ControlsRow>

      <ControlsRow header="output">
        <label htmlFor="fileextension">file extension</label>
        <select
          id="fileextension"
          onChange={e =>
            controlsDispatch({
              type: "SET_EXTENSION",
              payload: e.target.value
            })
          }
          value={controlsState.fileExtension}
        >
          <option value="gif">gif</option>
          <option value="mp4">mp4</option>
          <option value="avi">avi</option>
          <option value="webm">webm</option>
          <option value="mov">mov</option>
          <option value="flv">flv</option>
        </select>

        <label htmlFor="preset">quality preset</label>
        <select
          id="preset"
          onChange={e => {
            controlsDispatch({
              type: "SET_PRESET",
              payload: e.target.value as PresetValues
            });
          }}
          value={controlsState.preset}
        >
          <option value="ultrafast">ultrafast</option>
          <option value="superfast">superfast</option>
          <option value="veryfast">veryfast</option>
          <option value="faster">faster</option>
          <option value="fast">fast</option>
          <option value="medium">medium</option>
          <option value="slow">slow</option>
          <option value="slower">slower</option>
          <option value="veryslow">veryslow</option>
        </select>
      </ControlsRow>

      <ControlsRow header="hue and saturation">
        <label htmlFor="picture-hue">hue</label>
        <input
          id="picture-hue"
          type="number"
          min={0}
          max={360}
          value={controlsState.hue}
          onChange={e => {
            controlsDispatch({
              type: "SET_HUE",
              payload: Number(e.target.value)
            });
          }}
        />
        <label htmlFor="picture-saturation">saturation</label>
        <input
          id="picture-saturation"
          type="number"
          min={0}
          step={0.1}
          value={controlsState.saturation}
          onChange={e => {
            controlsDispatch({
              type: "SET_SATURATION",
              payload: Number(e.target.value)
            });
          }}
        />
      </ControlsRow>

      <ControlsRow header="flip">
        <label>
          <input
            id="flip-vertical"
            type="checkbox"
            value={controlsState.flip.vertical ? "checked" : ""}
            onChange={() => {
              controlsDispatch({
                type: "SET_FLIP",
                payload: { vertical: !controlsState.flip.vertical }
              });
            }}
          />
          vertical
        </label>
        <label>
          <input
            id="flip-horizontal"
            type="checkbox"
            value={controlsState.flip.horizontal ? "checked" : ""}
            onChange={() => {
              controlsDispatch({
                type: "SET_FLIP",
                payload: { horizontal: !controlsState.flip.horizontal }
              });
            }}
          />
          horizontal
        </label>
      </ControlsRow>

      <ControlsRow
        header={() => (
          <label>
            use cropper
            <input
              type="checkbox"
              value={controlsState.active.crop ? "checked" : ""}
              onChange={() => {
                controlsDispatch({
                  type: "SET_ACTIVE",
                  payload: { crop: !controlsState.active.crop }
                });
              }}
            />
          </label>
        )}
      >
        <div
          style={{
            display: controlsState.active.crop ? "block" : "none"
          }}
        >
          <Cropper />
        </div>
      </ControlsRow>
    </div>
  );
};

export default Controls;
