import React, { ChangeEvent, useRef, useEffect } from "react";
// import Range from "./components/Range";
import CropOverlay from "./components/CropOverlay";
import CommandView from "./components/CommandView";
import Filedetails from "./components/Filedetails";
import Controls from "./components/Controls";
import Multicut from "./components/Multicut";

import { getVideoImage } from "./utils/previews";
import { useControlsState, useControlsDispatch } from "./stores/controls";
import useVideo from "./hooks/useVideo";
import { CropProvider } from "./stores/crop";

import "./App.css";
import Seekbar from "./components/Seekbar";

const App: React.FC = () => {
  const controlsState = useControlsState();
  const controlsDispatch = useControlsDispatch();

  const fileSelect = useRef<HTMLInputElement>(null);
  const mainVideoRef = useRef<HTMLVideoElement>(null);

  const { isPlaying, play, pause, currentTime, seek } = useVideo(mainVideoRef);
  const started = Boolean(controlsState.fileMetadata);

  useEffect(() => {
    // const ESC = 27;
    const LEFT = 37;
    const RIGHT = 39;
    const keyHandler = ({ keyCode }: { keyCode: number }) => {
      if (!mainVideoRef.current) return;
      switch (keyCode) {
        // case ESC:
        //   closeAndLog('keyboard')();
        //   break;
        case LEFT:
          mainVideoRef.current.currentTime -= 1 / 30;

          break;
        case RIGHT:
          mainVideoRef.current.currentTime += 1 / 30;
          break;
        default:
          break;
      }
    };
    window.addEventListener("keydown", keyHandler);
    return () => {
      window.removeEventListener("keydown", keyHandler);
    };
  }, []);

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) handleFiles(e.target.files);
  };

  // const handleStartChange = (val: number) => {
  //   controlsDispatch({ type: "SET_RANGE_START", payload: val });
  // };
  // const handleEndChange = (val: number) => {
  //   controlsDispatch({ type: "SET_RANGE_END", payload: val });
  // };

  function handleFiles(files: FileList) {
    // !files[0] prevents error when opening upload window and then cancelling
    if (!fileSelect.current || !mainVideoRef.current || !files[0]) return;
    controlsDispatch({ type: "SET_MINIPREVIEWS", payload: [] });
    // for (let i = 0; i < files.length; i++) {

    mainVideoRef.current.onload = () => {
      if (!mainVideoRef.current) return;
      window.URL.revokeObjectURL(mainVideoRef.current.src);
    };

    mainVideoRef.current.onloadedmetadata = () => {
      if (!mainVideoRef.current) return;

      const { duration, videoWidth, videoHeight } = mainVideoRef.current;
      const name = files[0].name;

      controlsDispatch({
        type: "LOAD_FILE",
        payload: {
          name,
          duration,
          videoWidth,
          videoHeight
        }
      });

      const minipreviewHeight = 50;
      const minipreviewWidth = (videoWidth / videoHeight) * minipreviewHeight;
      const pieces = new Array(Math.ceil(window.innerWidth / minipreviewWidth))
        .fill(0)
        .map((_, i) => i);
      const times = pieces.map(i => (i / pieces.length) * duration);

      getVideoImage(mainVideoRef.current.src, times, minipreviewHeight).then(
        urls => {
          controlsDispatch({ type: "SET_MINIPREVIEWS", payload: urls });
        }
      );
    };

    mainVideoRef.current.src = window.URL.createObjectURL(files[0]);
  }

  // const timeToFract = (time: number) =>
  //   controlsState.fileMetadata ? time / controlsState.fileMetadata.duration : 0;

  return (
    <div className="App">
      <CropProvider>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            margin: "0 1rem 1rem"
          }}
        >
          <div
            style={{
              position: "relative",
              display: "inline-block",
              fontSize: 0
            }}
          >
            <video
              ref={mainVideoRef}
              controls={false}
              className="video-preview"
              style={{
                maxHeight: "90vh",
                transform: `scaleX(${
                  controlsState.flip.vertical ? -1 : 1
                }) scaleY(${controlsState.flip.horizontal ? -1 : 1})`,
                filter: `hue-rotate(${controlsState.hue}deg) saturate(${controlsState.saturation})`,
                maxWidth: "100%",
                display: started ? "initial" : "none"
              }}
            />
            {started && <CropOverlay show={controlsState.active.crop} />}
          </div>
        </div>
        {controlsState.fileMetadata && (
          <>
            <div className="video-controls">
              <button
                className="play-button"
                onClick={isPlaying ? pause : play}
              >
                <div className={isPlaying ? "icon-pause" : "icon-play"} />
              </button>
              <Seekbar
                videoSrc={mainVideoRef.current?.src}
                currentTime={currentTime}
                duration={controlsState.fileMetadata.duration}
                onCurrentTime={seek}
              />
            </div>
            {/* <Range
              onStart={handleStartChange}
              onEnd={handleEndChange}
              start={controlsState.range.start}
              end={controlsState.range.end}
              currentTime={currentTime}
              duration={controlsState.fileMetadata.duration}
              onCurrentTime={seek}
              videoSrc={mainVideoRef.current?.src}
              frameSizeInFract={timeToFract(1 / 30)}
            /> */}

            <Multicut videoSrc={mainVideoRef.current?.src} />
            <Controls />

            <div style={{ overflowX: "auto" }}>
              <CommandView />
            </div>
          </>
        )}
      </CropProvider>
      {!started && (
        <header className="u-tac">
          <p>
            This page will help with visually building a command for{" "}
            <a href="https://ffmpeg.org/" target="_blank" rel="noreferrer">
              ffmpeg
            </a>
          </p>
          <p>Start by selecting a file below</p>
        </header>
      )}
      <footer className="video-upload-and-details">
        <div className="u-tac">
          <input
            ref={fileSelect}
            type="file"
            multiple
            accept="video/*"
            className="video-input"
            onChange={onChange}
          />
          {!started && (
            <p>
              Nothing is being uploaded or processed on server - everything
              happens in the browser.
            </p>
          )}
        </div>
        <Filedetails />
      </footer>
    </div>
  );
};

export default App;
