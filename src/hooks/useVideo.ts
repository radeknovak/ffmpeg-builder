import { useEffect, useState } from "react";

const useVideo = (wrapper: React.MutableRefObject<HTMLVideoElement | null>) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [currentTime, setCurrentTime] = useState(0);

  const play = () => {
    wrapper.current?.play();
  };
  const pause = () => {
    wrapper.current?.pause();
  };
  const seek = (time: number) => {
    if (!wrapper.current) return;
    wrapper.current.currentTime = time;
  };

  useEffect(() => {
    const ref = wrapper.current;
    if (!ref) return;
    const _startPlaying = () => setIsPlaying(true);
    const _pausePlaying = () => setIsPlaying(false);
    const _updateTime = () => setCurrentTime(ref.currentTime ?? 0);
    ref.addEventListener("play", _startPlaying);
    ref.addEventListener("pause", _pausePlaying);
    ref.addEventListener("timeupdate", _updateTime);

    return () => {
      if (!ref) return;
      ref.removeEventListener("play", _startPlaying);
      ref.removeEventListener("pause", _pausePlaying);
      ref.removeEventListener("timeupdate", _updateTime);
    };
  }, [wrapper]);

  return { isPlaying, play, pause, currentTime, seek };
};

export default useVideo;
