import { useEffect, useRef } from "react";

const seekVals = (
  e: React.MouseEvent<HTMLElement, MouseEvent> | MouseEvent,
  bbox: DOMRect,
  window: Window
) => {
  const currentX = e.pageX - bbox.left - window.scrollX;
  const currentY = e.pageY - bbox.top - window.scrollY;
  let actualX = currentX;
  let actualY = currentY;

  if (actualX < 0) actualX = 0;
  if (actualY < 0) actualY = 0;
  if (actualX > bbox.width) actualX = bbox.width;
  if (actualY > bbox.height) actualY = bbox.height;

  const ratioX = actualX / bbox.width;
  const ratioY = actualY / bbox.height;

  return [ratioX, ratioY, actualX, actualY] as const;
};

const useMouseDrag = (
  onSet: (x: number, y: number, pixelX: number, pixelY: number) => void,
  wrapper: React.MutableRefObject<HTMLElement | null>
) => {
  const down = useRef(false);
  const localOnSet = useRef<
    (x: number, y: number, pixelX: number, pixelY: number) => void
  >(() => {});
  const bboxRef = useRef<DOMRect>();

  const onMouseDown = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    down.current = true;

    if (!wrapper.current || !down.current || !bboxRef.current) return;

    localOnSet.current(...seekVals(e, bboxRef.current, window));
  };

  useEffect(() => {
    localOnSet.current = onSet;
  }, [onSet]);

  useEffect(() => {
    const el = wrapper.current;
    if (!el) return;
    bboxRef.current = el.getBoundingClientRect();
    // @ts-ignore
    const resizeObserver = new ResizeObserver(() => {
      bboxRef.current = el.getBoundingClientRect();
    });

    resizeObserver.observe(el);

    return () => resizeObserver.unobserve(el);
  });

  useEffect(() => {
    const mouseMove = (e: MouseEvent) => {
      if (!wrapper.current || !down.current || !bboxRef.current) return;

      localOnSet.current(...seekVals(e, bboxRef.current, window));
    };

    const mouseUp = () => {
      down.current = false;
    };

    document.addEventListener("mousemove", mouseMove);
    document.addEventListener("mouseup", mouseUp);

    return () => {
      document.removeEventListener("mousemove", mouseMove);
      document.removeEventListener("mouseup", mouseUp);
    };
  }, [wrapper]);

  return onMouseDown;
};

export default useMouseDrag;
