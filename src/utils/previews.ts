export const getVideoImage = (
  videoSrc: string,
  secs: number[],
  height: number
) =>
  new Promise<string[]>((resolve, reject) => {
    const video = document.createElement("video");
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");
    const res = [] as string[];

    if (!ctx) return;

    const getImageURL = (time: number) =>
      new Promise<string>(resolve => {
        video.onseeked = () => {
          ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
          resolve(canvas.toDataURL());
        };

        video.currentTime = time;
      });

    video.onloadedmetadata = async function () {
      canvas.height = height;
      canvas.width = (video.videoWidth / video.videoHeight) * height;

      for (let sec of secs) {
        const image = await getImageURL(sec);
        res.push(image);
      }

      resolve(res);
    };

    video.onerror = function (e) {
      reject(e);
    };

    video.src = videoSrc;
  });
