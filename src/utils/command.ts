import { PresetValues } from "../stores/controls";
// ffmpeg [global_options] {[input_file_options] -i input_url} ... {[output_file_options] output_url} ...

const inputFile = (name: string) => `-i '${name}'`;
const outputFile = (shortName: string, ext: string) =>
  `'${shortName}-${Date.now()}.${ext}'`;
const startTime = (startTime: string | number) => `-ss ${startTime}`;
const endTime = (endTime: string | number) => `-to ${endTime}`;
const cropFilter = (params: {
  x: number;
  y: number;
  width: number;
  height: number;
}) => `crop=${params.width}:${params.height}:${params.x}:${params.y}`;

const hueFilter = (hue: number, saturation: number) =>
  hue === 0 && saturation === 1 ? "" : `hue=h=${hue}:s=${saturation}`;

const preset = (presetValue: PresetValues) =>
  presetValue === "medium" ? "" : `-preset ${presetValue}`;

const flipFilter = (v: boolean, h: boolean) => {
  let s = "";
  if (h) s += "vflip";
  if (v && h) s += ", ";
  if (v) s += "hflip";
  return s;
};

const filters = (...filters: string[]) => {
  const nonemptyFilters = filters.filter(Boolean);
  return nonemptyFilters.length ? `-vf "${nonemptyFilters.join(", ")}"` : "";
};

type Params = {
  name: string;
  shortName: string;

  ext: string;
  start: string | number;
  end: string | number;
  presetValue: PresetValues;
  flip: { vertical: boolean; horizontal: boolean };
  hueSat: { hue: number; saturation: number };
  crop: null | Parameters<typeof cropFilter>[0];
};

export const buildCommand = (params: Params) => {
  const {
    name,
    shortName,
    ext,
    start,
    end,
    crop,
    hueSat,
    flip,
    presetValue
  } = params;
  return [
    "ffmpeg",
    startTime(start),
    inputFile(name),
    filters(
      crop ? cropFilter(crop) : "",
      hueFilter(hueSat.hue, hueSat.saturation),
      flipFilter(flip.vertical, flip.horizontal)
    ),
    preset(presetValue),
    endTime(end),
    outputFile(shortName, ext)
  ]
    .filter(a => a !== "")
    .join(" ");
};
