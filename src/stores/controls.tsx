import * as React from "react";
import { createContext, FC, useReducer, useContext, Dispatch } from "react";

export type FileMeta = {
  name: string;
  duration: number;
  videoWidth: number;
  videoHeight: number;
};

export type PresetValues =
  | "ultrafast"
  | "superfast"
  | "veryfast"
  | "faster"
  | "fast"
  | "medium"
  | "slow"
  | "slower"
  | "veryslow";

type Action =
  | { type: "SET_EXTENSION"; payload: string }
  | { type: "SET_HUE"; payload: number }
  | { type: "SET_SATURATION"; payload: number }
  | { type: "SET_ACTIVE"; payload: { crop: boolean } }
  | {
      type: "SET_FLIP";
      payload: Partial<{ vertical: boolean; horizontal: boolean }>;
    }
  | { type: "DISABLE_RANGE"; payload: number }
  | { type: "REENABLE_RANGE"; payload: number }
  | { type: "SET_RANGE_START"; payload: number }
  | { type: "SET_RANGE_END"; payload: number }
  | { type: "SET_MINIPREVIEWS"; payload: string[] }
  | { type: "ADD_CUT_MARK"; payload: number }
  | { type: "DELETE_CUT_MARK"; payload: number }
  | { type: "MOVE_CUT_MARK"; payload: { index: number; value: number } }
  | { type: "LOAD_FILE"; payload: FileMeta }
  | { type: "SET_PRESET"; payload: PresetValues };

type State = typeof initialState;

const initialState = {
  fileExtension: "",
  hue: 0,
  saturation: 1,
  flip: { vertical: false, horizontal: false },
  preset: "medium" as PresetValues,
  active: {
    crop: false
  },
  shortName: "",
  marks: [] as number[],
  ranges: [[0, 1]] as [number, number][],
  disabledRanges: new Set<number>(),
  range: { start: 0, end: 1 },
  minipreviews: [] as string[],
  fileMetadata: null as FileMeta | null
};

const rangesFromMarks = (marks: State["marks"]) => {
  const boundedMarks = [0, ...marks, 1];
  const ranges: State["ranges"] = [];

  for (let i = 1; i < boundedMarks.length; i++) {
    ranges.push([boundedMarks[i - 1], boundedMarks[i]]);
  }

  return ranges;
};

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case "SET_EXTENSION":
      return { ...state, fileExtension: action.payload };
    case "LOAD_FILE":
      const split = action.payload.name.split(".");
      const shortName = split.slice(0, -1).join(".");
      const fileExtension = split[split.length - 1];

      return {
        ...state,
        fileMetadata: action.payload,
        shortName,
        fileExtension
      };
    case "SET_MINIPREVIEWS":
      return { ...state, minipreviews: action.payload };
    case "SET_SATURATION":
      return { ...state, saturation: action.payload };
    case "ADD_CUT_MARK": {
      const marks = Array.from(new Set([...state.marks, action.payload]));
      marks.sort();

      return {
        ...state,
        ranges: rangesFromMarks(marks),
        marks
      };
    }
    case "DELETE_CUT_MARK":
      const marks = [...state.marks];
      marks.sort();
      marks.splice(action.payload, 1);

      return {
        ...state,
        marks: marks,
        ranges: rangesFromMarks(marks)
      };
    case "MOVE_CUT_MARK": {
      const marks = [...state.marks];
      marks.sort();
      marks.splice(action.payload.index, 1, action.payload.value);

      return {
        ...state,
        marks: marks,
        ranges: rangesFromMarks(marks)
      };
    }
    case "SET_HUE":
      return { ...state, hue: action.payload };
    case "REENABLE_RANGE": {
      const disabledRanges = new Set(state.disabledRanges);
      disabledRanges.delete(action.payload);

      return {
        ...state,
        disabledRanges
      };
    }
    case "DISABLE_RANGE": {
      const disabledRanges = new Set(state.disabledRanges);
      disabledRanges.add(action.payload);
      return {
        ...state,
        disabledRanges
      };
    }
    case "SET_PRESET":
      return { ...state, preset: action.payload };
    case "SET_RANGE_START":
      return { ...state, range: { ...state.range, start: action.payload } };
    case "SET_RANGE_END":
      return { ...state, range: { ...state.range, end: action.payload } };
    case "SET_FLIP":
      return { ...state, flip: { ...state.flip, ...action.payload } };
    case "SET_ACTIVE":
      return { ...state, active: { ...state.active, ...action.payload } };
  }
};

//
// plumbing
//
const ControlsStateContext = createContext<undefined | typeof initialState>(
  undefined
);
const ControlsDispatchContext = createContext<undefined | Dispatch<Action>>(
  undefined
);

export const ControlsProvider: FC = props => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ControlsStateContext.Provider value={state}>
      <ControlsDispatchContext.Provider value={dispatch}>
        {props.children}
      </ControlsDispatchContext.Provider>
    </ControlsStateContext.Provider>
  );
};

export const useControlsState = () => {
  const context = useContext(ControlsStateContext);

  if (context === undefined) {
    throw new Error("useControlsState must be used within ControlsProvider");
  }

  return context;
};

export const useControlsDispatch = () => {
  const context = useContext(ControlsDispatchContext);

  if (context === undefined) {
    throw new Error("useControlsDispatch must be used within ControlsProvider");
  }

  return context;
};
