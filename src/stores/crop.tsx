import React from "react";
import { FileMeta } from "./controls";

type Point2d = {
  x: number;
  y: number;
  ratioX: number;
  ratioY: number;
};

const initialState = {
  topLeft: { x: 0, y: 0, ratioX: 0, ratioY: 0 } as Point2d,
  bottomRight: { x: 0, y: 0, ratioX: 1, ratioY: 1 } as Point2d,
  width: 0,
  height: 0
};

type Action =
  | {
      type: "set_top_left";
      payload: Point2d;
    }
  | {
      type: "set_bottom_right";
      payload: Point2d;
    };

const getWandH = (topLeft: Point2d, bottomRight: Point2d) => ({
  width: bottomRight.x - topLeft.x,
  height: bottomRight.y - topLeft.y
});

const reducer = (state: typeof initialState, action: Action) => {
  switch (action.type) {
    case "set_top_left": {
      return {
        ...state,
        topLeft: action.payload,
        ...getWandH(action.payload, state.bottomRight)
      };
    }
    case "set_bottom_right": {
      return {
        ...state,
        bottomRight: action.payload,
        ...getWandH(state.topLeft, action.payload)
      };
    }
  }
};

const CropContext = React.createContext([initialState, (a: Action) => null] as [
  typeof initialState,
  React.Dispatch<Action>
]);

export const CropProvider = (props: { children: React.ReactNode }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <CropContext.Provider value={[state, dispatch]}>
      {props.children}
    </CropContext.Provider>
  );
};

export const useCropContext = () => React.useContext(CropContext);

export const cropToVideoCoords = (
  { topLeft, bottomRight }: typeof initialState,
  fileMeta: FileMeta
) => ({
  x: Math.round(topLeft.ratioX * fileMeta.videoWidth),
  y: Math.round(topLeft.ratioY * fileMeta.videoHeight),
  width: Math.round(
    (bottomRight.ratioX - topLeft.ratioX) * fileMeta.videoWidth
  ),
  height: Math.round(
    (bottomRight.ratioY - topLeft.ratioY) * fileMeta.videoHeight
  )
});
