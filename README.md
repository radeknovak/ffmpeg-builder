### TODO

- [x] `-preset` options (https://trac.ffmpeg.org/wiki/Encode/H.264)
- [x] don't add default filters
- [x] fix use cropper
- [x] show exported segments with lengths
- [x] youtube style seek bar
- [x] offer not exporting segment
- [x] style ranges as boxes

- select between trim and cut
- scaling (https://trac.ffmpeg.org/wiki/Scaling)
- thumbnails
- download current frame
